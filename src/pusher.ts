import { SignalingChannel, SignalingMessage } from "./create";

interface PusherChannel {
    trigger(event: string, data: any): boolean;
    bind(eventName: string, callback: Function, context?: any): this;
    unbind(eventName?: string, callback?: Function, context?: any): this;
}

/**
 * Creates a SignalingChannel using a pusher channel.
 *
 * No peer negotation will happen - it is assumed that only the 2 peers listen
 * to messages on this pusher channel.
 */
export function createPusherSignalingChannel(channel: PusherChannel): SignalingChannel {
    return {
        async send(msg) {
            if (msg.type === 'description') {
                channel.trigger('client-description', msg.description)
            } else if (msg.type === 'candidate') {
                channel.trigger('client-candidate', msg.candidate)
            }
        },

        addEventListener(name, listener, options) {
            if (name !== 'message') {
                return
            }

            function callListener(msg: SignalingMessage) {
                listener(new MessageEvent(name, {
                    data: msg
                }))
            }

            function onDescription(description: RTCSessionDescription) {
                callListener({ type: 'description', description })
            }

            function onCandidate(candidate: RTCIceCandidateInit) {
                callListener({ type: 'candidate', candidate })
            }

            channel.bind('client-description', onDescription)
            channel.bind('client-candidate', onCandidate)

            options?.signal?.addEventListener('abort', () => {
                channel.unbind('client-description', onDescription)
                channel.unbind('client-candidate', onCandidate)
            })
        }
    }
}
