import { SignalingChannelOptions } from "./create"

/**
 * A safe size for individual data channel messages.
 */
/*
 * Other implementations like Pion (Go) might be limited to smaller amounts.
 * According to the peerjs source, there might also be an issue when sending from
 * Firefox to Chrome, but I could not confirm that myself.
 * PeerJS seems to be confused as well - in their stream implementation, they
 * use 32k as their chunk size instead. So what way is it?
 *
 * Between browsers, 64k seems fine according to my tests. I will use 16k here
 * to make sure Pion works.
 *
 * see https://github.com/peers/peerjs/blob/master/lib/dataconnection/BufferedConnection/binaryPackChunker.ts#L2
 * https://github.com/peers/peerjs/blob/master/lib/dataconnection/StreamConnection/StreamConnection.ts#L6
 */
export const safeMessageSize = 16384


/**
 * Creates a ReadableStream to read large amounts of data.
 *
 * Needs to be matched by a call to createWriteStream on the peer.
 *
 * Both peers exchange messages during the transfer to coordinate, and
 * messages need to be generally assumed to be in an arbitrary undefined
 * format that only a call to createWriteStream can handle.
 */
export function createReadStream(channel: RTCDataChannel): ReadableStream<Uint8Array> {
    if (!channel.ordered) {
        throw new Error('createReadStream requires an ordered channel')
    }

    // blob is not supported in chrome
    channel.binaryType = 'arraybuffer'

    const abort = new AbortController()

    return new ReadableStream({
        type: 'bytes',

        start(controller) {
            channel.addEventListener('close', () => {
                controller.close()
                abort.abort()
            }, { once: true, signal: abort.signal })

            channel.addEventListener('error', (ev) => {
                controller.error(ev)
                abort.abort()
            }, { once: true, signal: abort.signal })

            channel.addEventListener('message', (ev) => {
                if (!(ev.data instanceof ArrayBuffer)) {
                    return
                }

                const bytes = new Uint8Array(ev.data)
                if (bytes.byteLength) {
                    controller.enqueue(bytes)
                } else {
                    controller.close()
                    abort.abort()
                }
            }, { signal: abort.signal })
        },

        cancel() {
            abort.abort()
        }
    })
}

export interface WriteStreamOptions {
    /**
     * Use a specific chunk size. By default, `maxMessageSize` is used.
     */
    chunkSize?: number
}

/**
 * Create a WritableStream to write large amounts of data.
 *
 * Needs to be matched by a call to createReadStream on the peer.
 *
 * Both peers exchange messages during the transfer to coordinate, and
 * messages need to be generally assumed to be in an arbitrary undefined
 * format that only a call to createReadStream can handle.
 */
export function createWriteStream(channel: RTCDataChannel, options: WriteStreamOptions = {}): WritableStream<Uint8Array> {
    if (!channel.ordered) {
        throw new Error('createWriteStream requires an ordered channel')
    }

    const chunkSize = options.chunkSize || safeMessageSize
    if (chunkSize <= 0) {
        throw new Error('invalid chunkSize')
    }

    const bufferedSize = chunkSize * 8 * 2


    // blob is not supported in chrome
    channel.binaryType = 'arraybuffer'

    const abort = new AbortController()
    return new WritableStream({
        start(controller) {
            channel.addEventListener('close', () => {
                controller.error(new Error('channel closed'))
                abort.abort()
            }, { once: true, signal: abort.signal })

            channel.addEventListener('error', e => {
                controller.error(e)
                abort.abort()
            }, { once: true, signal: abort.signal })
        },

        async write(chunk) {
            if (!(chunk instanceof Uint8Array)) {
                throw new Error('createWriteStream only supports Uint8Array')
            }

            channel.bufferedAmountLowThreshold = bufferedSize / 2
            for (let offs = 0; offs < chunk.byteLength; offs += chunkSize) {
                const smallerChunk = chunk.subarray(offs, offs + chunkSize)

                if (channel.bufferedAmount + smallerChunk.byteLength > bufferedSize) {
                    // use channel here instead of events to make sure the promise resolves
                    await new Promise((res) => channel.addEventListener('bufferedamountlow', res, { once: true, signal: abort.signal }))
                }

                abort.signal.throwIfAborted()
                channel.send(smallerChunk)
            }
        },

        async close() {
            // this fixes FF sorting problems when calling send() multiple times per tick, depending on the type of data
            // and we need to make sure that we send CLOSE after the last DATA chunk
            channel.bufferedAmountLowThreshold = 1
            if (channel.bufferedAmount > channel.bufferedAmountLowThreshold) {
                await new Promise(res => channel.addEventListener('bufferedamountlow', res, { once: true, signal: abort.signal }))
            }

            abort.abort()

            // wait an extra tick just to be sure
            await new Promise(res => setTimeout(res, 0))

            channel.send(new Uint8Array([]))
        },

        async abort() {
            abort.abort()
            channel.send(new Uint8Array([]))
        },
    })
}

/**
 * Send an arbitrarily big blob over a data channel.
 *
 * Needs to be matched by a call to receiveBlob or createReadStream on the peer.
 */
export function sendBlob(channel: RTCDataChannel, blob: Blob, options?: SignalingChannelOptions & WriteStreamOptions): Promise<void> {
    const writeStream = createWriteStream(channel, options)
    return blob.stream().pipeTo(writeStream, { signal: options?.signal })
}

/**
 * Receive an arbitrarly big blob over a data channel.
 *
 * Needs to be matched by a call to sendBlob or createWriteStream on the peer.
 */
export async function receiveBlob(channel: RTCDataChannel, options?: SignalingChannelOptions): Promise<Blob> {
    const stream = createReadStream(channel)
    const parts: BlobPart[] = []

    // for some reason, readable stream is not an async iterable. So we do it manually
    const reader = stream.getReader()
    while (true) {
        const { done, value } = await reader.read()
        options?.signal?.throwIfAborted()
        if (done) {
            break
        }

        parts.push(value)
    }

    return new Blob(parts)
}

/**
 * Send an arbitrarily big string over a data channel.
 *
 * Needs to be matched by a call to receiveString on the peer.
 */
export async function sendString(channel: RTCDataChannel, string: string, options?: SignalingChannelOptions & WriteStreamOptions): Promise<void> {
    const encoderStream = new TextEncoderStream()
    const writeStream = createWriteStream(channel, options)

    let offs = 0
    const maxStringChunkSize = (options?.chunkSize || safeMessageSize) / 2 // half because of utf-16
    const stringStream = new ReadableStream({
        start() {
            offs = 0
        },

        pull(controller) {
            const chunk = string.substring(offs, maxStringChunkSize)
            offs += maxStringChunkSize

            controller.enqueue(chunk)
            if (offs >= string.length) {
                controller.close()
            }
        }
    })

    await Promise.all([
        stringStream.pipeTo(encoderStream.writable, { signal: options?.signal }),
        encoderStream.readable.pipeTo(writeStream, { signal: options?.signal })
    ])
}

/**
 * Receive an arbitrarily big string over a data channel.
 *
 * Needs to be matched by a call to sendString on the peer.
 */
export async function receiveString(channel: RTCDataChannel, options?: SignalingChannelOptions): Promise<string> {
    const decoderStream = new TextDecoderStream()
    const readStream = createReadStream(channel)

    const chunks: string[] = []
    const stringStream = new WritableStream({
        write(chunk: string) {
            chunks.push(chunk)
        }
    })

    await Promise.all([
        readStream.pipeTo(decoderStream.writable, { signal: options?.signal }),
        decoderStream.readable.pipeTo(stringStream, { signal: options?.signal })
    ])

    return chunks.join('')
}
