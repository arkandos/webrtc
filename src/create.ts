export type SignalingMessage =
    | { type: 'candidate', candidate: RTCIceCandidateInit }
    | { type: 'description', description: RTCSessionDescriptionInit }

export interface SignalingChannelOptions {
    signal?: AbortSignal
}

export interface SignalingChannel {
    send(msg: SignalingMessage): Promise<void>;
    addEventListener(name: 'message', listener: (event: MessageEvent<SignalingMessage>)
    => void, options?: SignalingChannelOptions): void;
}

/**
 * all unknown properties will be forwarded to the RTCPeerConnection constructor.
 */
export interface CreateConfiguration extends RTCConfiguration, SignalingChannelOptions {
    signaling: SignalingChannel

    /**
     * If set, ICE will automatically be restarted if it fails.
     */
    autoRestartIce?: boolean

    /**
     * If set, request microphone access before ICE candidate gathering starts.
     *
     * Browsers seem to generate "better" ICE candidates when there is a media
     * stream opened.
     *
     * The opene microphone streams will be closed automatically once ICE negotation completes.
     */
    useMicrophone?: boolean
}

/**
 * Create a RTCPeerConnection, setting up perfect negotation using a signaling channel.
 */
export function createPeerConnection(configuration: CreateConfiguration): RTCPeerConnection {
    const {
        signaling,
        autoRestartIce,
        useMicrophone
    } = configuration

    const { signal } = configuration
    const pc = new RTCPeerConnection(configuration)

    async function setLocalDescription() {
        await pc.setLocalDescription()
        if (!pc.localDescription) {
            throw new Error('no localDescription after setLocalDecsription')
        }

        const description = { sdp: pc.localDescription.sdp, type: pc.localDescription.type }
        await signaling.send({ type: 'description', description })
    }

    pc.addEventListener('negotiationneeded', setLocalDescription, { signal })

    pc.addEventListener('icecandidate', async ({ candidate }) => {
        if (candidate) {
            await signaling.send({ type: 'candidate', candidate })
        }
    }, { signal })

    pc.addEventListener('iceconnectionstatechange', () => {
        if (pc.iceConnectionState === 'failed') {
            if (autoRestartIce) {
                pc.restartIce()
            }
        }
    }, { signal })

    pc.addEventListener('icegatheringstatechange', async () => {
        if (pc.iceGatheringState === 'complete') {
            await stopMicrophone()
        } else {
            if (useMicrophone) {
                await startMicrophone()
            }
        }
    }, { signal })

    signaling.addEventListener('message', async ({ data: msg }) => {
        if (msg.type === 'description') {
            await pc.setRemoteDescription(msg.description)
            if (msg.description.type === 'offer') {
                await setLocalDescription()
            }
        } else if (msg.type === 'candidate') {
            await pc.addIceCandidate(msg.candidate)
        }
    }, { signal })

    signal?.addEventListener('abort', function() {
        pc.close()
        stopMicrophone()
    })

    return pc
}


/**
 * Open a data channel, resolving once the channel reaches the 'open' state
 */
export function createDataChannel(pc: RTCPeerConnection, options?: RTCDataChannelInit & SignalingChannelOptions): Promise<RTCDataChannel> {
    const channel = pc.createDataChannel(crypto.randomUUID(), options)
    const signal = options?.signal

    signal?.addEventListener('abort', () => channel.close())

    return new Promise((res, rej) => {
        channel.addEventListener('open', () => res(channel), { once: true, signal })
        channel.addEventListener('error', rej, { once: true, signal })
        signal?.addEventListener('abort', rej, { once: true })
    })
}


let stream: MediaStream|null = null
async function startMicrophone() {
    if (stream) {
        return
    }

    stream = await navigator.mediaDevices.getUserMedia({ audio: true })
}

async function stopMicrophone() {
    if (!stream) {
        return
    }

    stream.getTracks().forEach(track => track.stop())

    stream = null
}
