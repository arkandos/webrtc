[webrtc-adapter](https://github.com/webrtcHacks/adapter) includes many shims to reduce browser inconsistencies. Most of them don't affect me or are outdated. `webrtc-adapter` is also quite heavy.

Here, I catalog all the shims they provide (as of Sep 2023), and reason about what to do with them.

I extracted the list of shims that are applied per browser from [adapter_factory.js](https://github.com/webrtcHacks/adapter/blob/main/src/js/adapter_factory.js).

If the shim is only applied for certain browser versions, I check if those are recent.
If the shim checks for properties, I checked if my version had that property and what caniuse says.
Almost all the things are no longer needed in current browsers or just fix legacy APIs.

## Common

 - [x] commonShim.removeExtmapAllowMixed
 - [x] commonShim.shimConnectionState
 - [x] commonShim.shimAddIceCandidateNullOrEmpty
 - [x] commonShim.shimParameterlessSetLocalDescription
        No longer needed in current versions

 - [x] commonShim.shimRTCIceCandidateRelayProtocol
 - [x] commonShim.shimRTCIceCandidate
        this parses the candidate using SDPUtils, and adds the result as reaedonly
        properties. If you need that, just call SDPUtils yourself.

 - [x] commonShim.shimMaxMessageSize
       Just use 64k

 - [x] commonShim.shimSendThrowTypeError
        Overrides RTCDataChannel.send, throwing a type error if the message is
        too big. I'm not into that, just let people be stupid if they want to
        (people is myself only anyways)

## Chrome

 - [x] chromeShim.shimPeerConnection
 - [x] chromeShim.shimMediaStream
 - [x] chromeShim.shimOnTrack
 - [x] chromeShim.shimGetSendersWithDtmf
 - [x] chromeShim.shimSenderReceiverGetStats
 - [x] chromeShim.fixNegotiationNeeded
 - [x] chromeShim.shimGetUserMedia
 - [x] chromeShim.shimGetStats
    No longer needed in current versions

 - [x] chromeShim.shimAddTrackRemoveTrack
    adds fixes for a deprecated API


## Firefox

 - [x] firefoxShim.shimGetUserMedia
 - [x] firefoxShim.shimPeerConnection
 - [x] firefoxShim.shimOnTrack
 - [x] firefoxShim.shimSenderGetStats
 - [x] firefoxShim.shimReceiverGetStats
 - [x] firefoxShim.shimRTCDataChannel
 - [x] firefoxShim.shimAddTransceiver
 - [x] firefoxShim.shimCreateOffer
 - [x] firefoxShim.shimCreateAnswer
    No longer needed in current versions

 - [x] firefoxShim.shimRemoveStream
    deprecated API

 - [x] firefoxShim.shimGetParameters
    I THINK this is no longer needed - it adds the `encodings` prop to the return
    value if it does not exist.

## Safari

 - [x] safariShim.shimCallbacksAPI
 - [x] safariShim.shimRTCIceServerUrls
 - [x] safariShim.shimLocalStreamsAPI
 - [x] safariShim.shimRemoteStreamsAPI
    deprecated API

 - [x] safariShim.shimAudioContext
    No longer needed

 - [x] safariShim.shimCreateOfferLegacy
    offerToReceive* this is deprecated

 - [?] safariShim.shimTrackEventTransceiver
    caniuse says this is no longer needed, but I can't test (BrowserStack?)
    it just renames receiver to transceiver, if transceiver is not defined

 - [x] safariShim.shimGetUserMedia
    this removes undefineds/empty objects from the video object constraint -
    just don't pass silly things

