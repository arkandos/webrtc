My personal collection of WebRTC helper functions.

Create a connection using perfect forwarding, send arbitrarily large things, use Pusher as a signaling channel.
